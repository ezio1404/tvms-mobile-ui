package com.example.tvmsph;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class AddViolationActivity extends AppCompatActivity  implements ViolatorOrdinanceListAdapter.OnViolationListener{
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private CollectionReference agencyRef = db.collection("agency");
    private RecyclerView mRecyclerView;
    private ViolatorOrdinanceListAdapter mAdapter;
    private List<ViolatorOrdinance> mList;
    private ArrayList<ViolatorOrdinance> mListCheckeditems = new ArrayList<>();
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_violation);
        fab = findViewById(R.id.btnAddViolation);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AddViolationActivity.this, "Total Items checked :" + mAdapter.getCheckeditemsCount() , Toast.LENGTH_SHORT).show();
                if(mAdapter.getCheckeditemsCount() >0){
                    startActivity(new Intent(AddViolationActivity.this,violator.class));
                    mList.clear();
                }else{
                    Toast.makeText(AddViolationActivity.this, "Please check one or more violation", Toast.LENGTH_SHORT).show();
                }
            }
        });
        setUpViolatorOrdinance();
    }

    private void setUpViolatorOrdinance() {

        mRecyclerView = findViewById(R.id.recycler_view_ordinance_violator);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mList = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> agency = new ArrayList<>();

                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agency.add(doc.getString("agency_name"));

                                agencyRef.document(doc.getId()).collection("enforcer")
                                        .whereEqualTo("status", "enforcer")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @SuppressLint("SetTextI18n")
                                            @Override
                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    System.err.println("Listen failed:" + e);
                                                    return;
                                                }
                                                List<String> enforcerList = new ArrayList<>();
                                                for (DocumentSnapshot enfDoc: snapshots) {
                                                    Enforcer enforcer= enfDoc.toObject(Enforcer.class);
                                                    if (enfDoc.get("enforcer_lname") != null) {
                                                        enforcerList.add(enfDoc.getId());
                                                        if(enfDoc.getId().equals(user.getUid())){
                                                            //////////////////////////////
                                                            agencyRef.document(doc.getId()).collection("ordinance")
                                                                    .whereEqualTo("status", "active")
                                                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                        @SuppressLint("SetTextI18n")
                                                                        @Override
                                                                        public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                            if (e != null) {
                                                                                System.err.println("Listen failed:" + e);
                                                                                return;
                                                                            }
                                                                            List<String> ordinanceList = new ArrayList<>();
                                                                            for (DocumentSnapshot ordDoc : snapshots) {
                                                                                if (ordDoc.get("violation_article") != null) {
                                                                                    ViolatorOrdinance violatorOrdinance = ordDoc.toObject(ViolatorOrdinance.class);
                                                                                    ordinanceList.add(ordDoc.getId());
                                                                                    violatorOrdinance.setViolation_uid(ordDoc.getId());
                                                                                    mList.add(violatorOrdinance);
                                                                                }
                                                                            }
                                                                            mAdapter = new ViolatorOrdinanceListAdapter(AddViolationActivity.this, mList,AddViolationActivity.this);

                                                                            mRecyclerView.setAdapter(mAdapter);
                                                                            System.out.println("Current violation active: " + mList);
                                                                        }
                                                                    });
                                                            /////////////////////////////
                                                        }
                                                    }
                                                }
//                                                System.out.println("Current enforcer : " + enforcerList);
                                            }
                                        });

                            }
                        }
//                        System.out.println("Current Agency active: " + agency);
                    }
                });
            } else {

            }
        };
    }

    @Override
    public void finish() {
        super.finish();
        mList.clear();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener(mAuthListener);
        mList.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onViolationClick(View v,int position) {
        ViolatorOrdinance CurrentViolatorOrdinance =(ViolatorOrdinance) mList.get(position);
        Toast.makeText(this, "desc :" + CurrentViolatorOrdinance.getViolation_desc(), Toast.LENGTH_SHORT).show();
    }
}
