package com.example.tvmsph;

public class Citizen {

    private String citizen_fname;
    private String citizen_lname;
    private String citizen_mi;

    private String citizen_addr;
    private String citizen_city;
    private String citizen_postal;

    private String citizen_email;
    private String citizen_status;

    private String citizen_phone;
    private String citizen_dateCreated;

    public Citizen() {
    }
    public Citizen(String citizen_fname, String citizen_lname, String citizen_mi, String citizen_addr, String citizen_city, String citizen_postal, String citizen_email, String citizen_phone, String citizen_status, String citizen_dateCreated) {
        this.citizen_fname = citizen_fname;
        this.citizen_lname = citizen_lname;
        this.citizen_mi = citizen_mi;

        this.citizen_addr = citizen_addr;
        this.citizen_city = citizen_city;
        this.citizen_postal = citizen_postal;

        this.citizen_email = citizen_email;
        this.citizen_phone = citizen_phone;
        this.citizen_status =citizen_status;
        this.citizen_dateCreated = citizen_dateCreated;
    }

    public String getCitizen_fname() {
        return citizen_fname;
    }

    public void setCitizen_fname(String citizen_fname) {
        this.citizen_fname = citizen_fname;
    }

    public String getCitizen_lname() {
        return citizen_lname;
    }

    public void setCitizen_lname(String citizen_lname) {
        this.citizen_lname = citizen_lname;
    }

    public String getCitizen_mi() {
        return citizen_mi;
    }

    public void setCitizen_mi(String citizen_mi) {
        this.citizen_mi = citizen_mi;
    }

    public String getCitizen_addr() {
        return citizen_addr;
    }

    public void setCitizen_addr(String citizen_addr) {
        this.citizen_addr = citizen_addr;
    }

    public String getCitizen_city() {
        return citizen_city;
    }

    public void setCitizen_city(String citizen_city) {
        this.citizen_city = citizen_city;
    }

    public String getCitizen_postal() {
        return citizen_postal;
    }

    public void setCitizen_postal(String citizen_postal) {
        this.citizen_postal = citizen_postal;
    }

    public String getCitizen_email() {
        return citizen_email;
    }

    public void setCitizen_email(String citizen_email) {
        this.citizen_email = citizen_email;
    }


    public String getCitizen_phone() {
        return citizen_phone;
    }

    public void setCitizen_phone(String citizen_phone) {
        this.citizen_phone = citizen_phone;
    }

    public String getCitizen_status() {
        return citizen_status;
    }

    public void setCitizen_status(String citizen_status) {
        this.citizen_status = citizen_status;
    }

    public String getCitizen_dateCreated() {
        return citizen_dateCreated;
    }

    public void setCitizen_dateCreated(String citizen_dateCreated) {
        this.citizen_dateCreated = citizen_dateCreated;
    }
}
