package com.example.tvmsph;

public class Contact {

    private String contact_name;
    private String contact_number;
    private String contact_type;

    // empty constructor
    public Contact() {

    }

    public Contact(String contact_name, String contact_number, String contact_type) {
        this.contact_name = contact_name;
        this.contact_number = contact_number;
        this.contact_type = contact_type;
    }

    public String getContact_number() {
        return contact_number;
    }

    public String getContact_type() {
        return contact_type;
    }

    public String getContact_name() {
        return contact_name;
    }


}
