package com.example.tvmsph;

public class Driver {
    private String driver_addr;
    private String driver_city;
    private String driver_postal;
    private String driver_fname;
    private String driver_lname;
    private String driver_mi;
    private String driver_email;
    private String driver_phone;
    private String license_agency;
    private String license_expiry;
    private String license_no;
    private String license_restriction;
    private String status;
    public Driver(){

    }
    public Driver(String driver_addr, String driver_city, String driver_postal, String driver_fname, String driver_lname, String driver_mi, String driver_email, String driver_phone, String license_agency, String license_expiry, String license_no, String license_restriction, String status) {
        this.driver_addr = driver_addr;
        this.driver_city = driver_city;
        this.driver_postal = driver_postal;
        this.driver_fname = driver_fname;
        this.driver_lname = driver_lname;
        this.driver_mi = driver_mi;
        this.driver_email = driver_email;
        this.driver_phone = driver_phone;
        this.license_agency = license_agency;
        this.license_expiry = license_expiry;
        this.license_no = license_no;
        this.license_restriction = license_restriction;
        this.status = status;
    }

    public String getDriver_addr() {
        return driver_addr;
    }

    public void setDriver_addr(String driver_addr) {
        this.driver_addr = driver_addr;
    }

    public String getDriver_city() {
        return driver_city;
    }

    public void setDriver_city(String driver_city) {
        this.driver_city = driver_city;
    }

    public String getDriver_postal() {
        return driver_postal;
    }

    public void setDriver_postal(String driver_postal) {
        this.driver_postal = driver_postal;
    }

    public String getDriver_fname() {
        return driver_fname;
    }

    public void setDriver_fname(String driver_fname) {
        this.driver_fname = driver_fname;
    }

    public String getDriver_lname() {
        return driver_lname;
    }

    public void setDriver_lname(String driver_lname) {
        this.driver_lname = driver_lname;
    }

    public String getDriver_mi() {
        return driver_mi;
    }

    public void setDriver_mi(String driver_mi) {
        this.driver_mi = driver_mi;
    }

    public String getDriver_email() {
        return driver_email;
    }

    public void setDriver_email(String driver_email) {
        this.driver_email = driver_email;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }

    public String getLicense_agency() {
        return license_agency;
    }

    public void setLicense_agency(String license_agency) {
        this.license_agency = license_agency;
    }

    public String getLicense_expiry() {
        return license_expiry;
    }

    public void setLicense_expiry(String license_expiry) {
        this.license_expiry = license_expiry;
    }

    public String getLicense_no() {
        return license_no;
    }

    public void setLicense_no(String license_no) {
        this.license_no = license_no;
    }

    public String getLicense_restriction() {
        return license_restriction;
    }

    public void setLicense_restriction(String license_restriction) {
        this.license_restriction = license_restriction;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
