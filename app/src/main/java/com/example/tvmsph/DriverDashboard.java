package com.example.tvmsph;

import android.content.Intent;
import android.os.Bundle;


import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DriverDashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PostAdapter.OnPostListener {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private ProgressBar mProgresbar;
    private RecyclerView mRecyclerView;
    private PostAdapter mAdapter;
    private List<Post> mPost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home / Driver");

        mProgresbar= findViewById(R.id.progress_bar_driver_post);
        mRecyclerView = findViewById(R.id.recycler_view_post_driver);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mPost = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                agencyRef.whereEqualTo("subscription_status","active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> agency = new ArrayList<>();
                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agency.add(doc.getString("agency_name"));
                                agencyRef.document(doc.getId()).collection("post")
                                        .whereEqualTo("post_status", "active")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @Override
                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    System.err.println("Listen failed:" + e);
                                                    return;
                                                }
                                                List<String> postList = new ArrayList<>();
                                                for (DocumentSnapshot docEnf : snapshots) {
                                                    if (docEnf.get("post_title") != null) {
                                                        postList.add(docEnf.getId());
                                                        Post post= docEnf.toObject(Post.class);
                                                        post.setAgency_name(doc.getString("agency_name"));
                                                        mPost.add(post);
                                                    }
                                                }
                                                mAdapter= new PostAdapter(DriverDashboard.this,mPost,DriverDashboard.this);
                                                mRecyclerView.setAdapter(mAdapter);
                                                mProgresbar.setVisibility(View.GONE);
                                                System.out.println("Agency : "+doc.getString("agency_name")+"Current post active: " + postList);
                                            }
                                        });
                            }
                        }
                        System.out.println("Current Agency active: " + agency);
                    }

                });


            } else {

            }
        };

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.driver_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_violation) {
            Intent violation = new Intent(this, violation.class);
            startActivity(violation);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (id == R.id.nav_inquiry) {
            startActivity(new Intent(this, inquiry.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (id == R.id.nav_vp) {
            startActivity(new Intent(this, ordinance.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (id == R.id.nav_ra) {
            startActivity(new Intent(this, laws.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (id == R.id.nav_help) {
            startActivity(new Intent(this, help.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(this, profile.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (id == R.id.nav_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(DriverDashboard.this, LoginActivity.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onPostClick(int position) {

    }
}

////                status.setText(user.getUid());
//                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
//@Override
//public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
//        if (e != null) {
//        System.err.println("Listen failed:" + e);
//        return;
//        }
//        List<String> agency = new ArrayList<>();
//
//        for (DocumentSnapshot doc : snapshots) {
//        if (doc.get("agency_name") != null) {
//        agency.add(doc.getString("agency_name"));
//
//        agencyRef.document(doc.getId()).collection("driver")
//        .whereEqualTo("status", "driver")
//        .addSnapshotListener(new EventListener<QuerySnapshot>() {
//@Override
//public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
//        if (e != null) {
//        System.err.println("Listen failed:" + e);
//        return;
//        }
//        List<String> driverList = new ArrayList<>();
//        for (DocumentSnapshot doc : snapshots) {
//        Driver driver = doc.toObject(Driver.class);
//        if (doc.get("driver_lname") != null) {
//        driverList.add(doc.getId());
//        if (doc.getId().equals(user.getUid())) {
//        status.setText(user.getUid());
//
//        }
//        }
//        }
//
////                                                test = status.getText().toString();
////                                                System.out.println("test : " + test);
////                                                if (test.equals("TextView")) {
////                                                    FirebaseAuth.getInstance().signOut();
////                                                    startActivity(new Intent(DriverDashboard.this, LoginActivity.class));
////                                                }
//
//        System.out.println("Current Driver active: " + driverList);
//        }
//        });
//
//        }
//        }
//        System.out.println("Current Agency active: " + agency);
//        }
//        });