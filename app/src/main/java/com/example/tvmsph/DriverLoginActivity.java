package com.example.tvmsph;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DriverLoginActivity extends AppCompatActivity {
    private static final int MY_REQUEST_CODE = 123;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG = "DriverLoginActivity";
    private TextView btnEnforcer,btnCitizen;
    private ProgressBar progress_bar_driver_login;
    EditText mEmail;
    EditText mPassword;
    Button mLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_login);
        progress_bar_driver_login=findViewById(R.id.progress_bar_driver_login);
        btnEnforcer = findViewById(R.id.btnEnforcerLoginDriver);
        btnEnforcer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DriverLoginActivity.this , LoginActivity.class));
            }
        });
        btnCitizen = findViewById(R.id.btnCitizenLoginDriver);
        btnCitizen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DriverLoginActivity.this , CitizenLoginActivity.class));
            }
        });
        mEmail = findViewById(R.id.mEmailDriver);
        mPassword = findViewById(R.id.mPasswordDriver);
        mLogin = findViewById(R.id.mLoginDriver);
        mAuth = FirebaseAuth.getInstance();


        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress_bar_driver_login.setVisibility(View.VISIBLE);
                mLogin.setVisibility(View.GONE);
                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                if (email.isEmpty()) {
                    mEmail.setError("Please Enter An Email");
                    mEmail.requestFocus();
                }
                if (password.isEmpty()) {
                    mPassword.setError("Please Enter Password");
                    mPassword.requestFocus();
                } else if (email.isEmpty() && password.isEmpty()) {
                    Toast.makeText(DriverLoginActivity.this, "Fields are Empty", Toast.LENGTH_LONG).show();

                } else if (!email.isEmpty() && !password.isEmpty()) {
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(DriverLoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    progress_bar_driver_login.setVisibility(View.GONE);
                                    mLogin.setVisibility(View.VISIBLE);
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "signInWithEmail:success");

                                        Intent driverLoginIntent = new Intent(DriverLoginActivity.this, DriverDashboard.class);
                                        startActivity(driverLoginIntent);


                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                                        Toast.makeText(DriverLoginActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                    // ...
                                }
                            });
                } else {
                    Toast.makeText(DriverLoginActivity.this, "An Error occured", Toast.LENGTH_LONG).show();

                }
            }
        });


    }

}
