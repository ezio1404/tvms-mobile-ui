package com.example.tvmsph;

public class Enforcer {
    private String enforcer_addr;
    private String enforcer_bday;
    private String enforcer_city;
    private String enforcer_postal;
    private String enforcer_email;
    private String enforcer_fname;
    private String enforcer_lname;
    private String enforcer_mi;
    private String enforcer_phone;
    private String status;


    public Enforcer() {

    }

    public Enforcer(String enforcer_addr, String enforcer_bday, String enforcer_city, String enforcer_postal, String enforcer_email, String enforcer_fname, String enforcer_lname, String enforcer_mi, String enforcer_phone, String status) {
        this.enforcer_addr = enforcer_addr;
        this.enforcer_bday = enforcer_bday;
        this.enforcer_city = enforcer_city;
        this.enforcer_postal = enforcer_postal;
        this.enforcer_email = enforcer_email;
        this.enforcer_fname = enforcer_fname;
        this.enforcer_lname = enforcer_lname;
        this.enforcer_mi = enforcer_mi;
        this.enforcer_phone = enforcer_phone;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEnforcer_phone() {
        return enforcer_phone;
    }

    public void setEnforcer_phone(String enforcer_phone) {
        this.enforcer_phone = enforcer_phone;
    }

    public String getEnforcer_mi() {
        return enforcer_mi;
    }

    public void setEnforcer_mi(String enforcer_mi) {
        this.enforcer_mi = enforcer_mi;
    }

    public String getEnforcer_lname() {
        return enforcer_lname;
    }

    public void setEnforcer_lname(String enforcer_lname) {
        this.enforcer_lname = enforcer_lname;
    }

    public String getEnforcer_fname() {
        return enforcer_fname;
    }

    public void setEnforcer_fname(String enforcer_fname) {
        this.enforcer_fname = enforcer_fname;
    }

    public String getEnforcer_email() {
        return enforcer_email;
    }

    public void setEnforcer_email(String enforcer_email) {
        this.enforcer_email = enforcer_email;
    }

    public String getEnforcer_postal() {
        return enforcer_postal;
    }

    public void setEnforcer_postal(String enforcer_postal) {
        this.enforcer_postal = enforcer_postal;
    }

    public String getEnforcer_bday() {
        return enforcer_bday;
    }

    public void setEnforcer_bday(String enforcer_bday) {
        this.enforcer_bday = enforcer_bday;
    }

    public String getEnforcer_addr() {
        return enforcer_addr;
    }

    public void setEnforcer_addr(String enforcer_addr) {
        this.enforcer_addr = enforcer_addr;
    }

    public String getEnforcer_city() {
        return enforcer_city;
    }

    public void setEnforcer_city(String enforcer_city) {
        this.enforcer_city = enforcer_city;
    }
}
