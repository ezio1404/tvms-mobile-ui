package com.example.tvmsph;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class LTO_services extends AppCompatActivity {
    Button btn_nnp, btn_rnp, btn_ns, btn_nrp, btn_rrp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lto_services);
        getSupportActionBar().setTitle("LTO services");


        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle(getString(R.string.stringLTO));
        btn_nnp = findViewById(R.id.btn_nnp);
        btn_rnp = findViewById(R.id.btn_rnp);
        btn_ns = findViewById(R.id.btn_ns);
        btn_nrp = findViewById(R.id.btn_nrp);
        btn_rrp = findViewById(R.id.btn_rrp);

        btn_nnp.setOnClickListener(v1 -> {
            Uri uri = Uri.parse("smsto:2600");
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            smsIntent.putExtra("sms_body", "LTO NEW NON PRO");
            startActivity(smsIntent);
        });
        btn_rnp.setOnClickListener(v1 -> {
            Uri uri = Uri.parse("smsto:2600");
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            smsIntent.putExtra("sms_body", "LTO RENEW NON PRO");
            startActivity(smsIntent);
        });
        btn_ns.setOnClickListener(v1 -> {
            Uri uri = Uri.parse("smsto:2600");
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            smsIntent.putExtra("sms_body", "LTO NEW STUDENT");
            startActivity(smsIntent);
        });
        btn_nrp.setOnClickListener(v1 -> {
            Uri uri = Uri.parse("smsto:2600");
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            smsIntent.putExtra("sms_body", "LTO NEW REG PRIVATE");
            startActivity(smsIntent);
        });
        btn_rrp.setOnClickListener(v1 -> {
            Uri uri = Uri.parse("smsto:2600");
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            smsIntent.putExtra("sms_body", "LTO RENEW REG PRIVATE");
            startActivity(smsIntent);
        });
        mBuilder.setPositiveButton("Ok",null);
        AlertDialog dialog = mBuilder.create();
        dialog.show();

    }


}
