package com.example.tvmsph;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

import org.w3c.dom.Text;

public class OrdinanceAdapter extends FirestoreRecyclerAdapter<ordinance_agency,OrdinanceAdapter.OrdinanceHolder> {
    private OnItemClickListener listener;

    public OrdinanceAdapter(@NonNull FirestoreRecyclerOptions<ordinance_agency> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull OrdinanceHolder ordinanceHolder, int i, @NonNull ordinance_agency ordinance_agency) {
        ordinanceHolder.agency_name.setText(ordinance_agency.getAgency_name());
        ordinanceHolder.agency_head.setText(ordinance_agency.getAgency_head());

    }

    @NonNull
    @Override
    public OrdinanceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ordinance_item,
                parent, false);
        return new OrdinanceHolder(v);
    }

    class OrdinanceHolder extends RecyclerView.ViewHolder{
        TextView agency_name;
        TextView agency_head;

        public OrdinanceHolder(@NonNull View itemView) {
            super(itemView);
            agency_name = itemView.findViewById(R.id.agency_name);
            agency_head = itemView.findViewById(R.id.agency_head);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener !=null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
