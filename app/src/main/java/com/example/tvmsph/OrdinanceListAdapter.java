package com.example.tvmsph;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class OrdinanceListAdapter extends FirestoreRecyclerAdapter<OrdinanceList, OrdinanceListAdapter.OrdinanceListHolder> {


    public OrdinanceListAdapter(@NonNull FirestoreRecyclerOptions<OrdinanceList> options) {
        super(options);
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void onBindViewHolder(@NonNull OrdinanceListHolder ordinanceListHolder, int i, @NonNull OrdinanceList ordinanceList) {
        ordinanceListHolder.violation_article.setText(ordinanceList.getViolation_article());
        ordinanceListHolder.violation_penalty.setText(ordinanceList.getViolation_penalty()+".00");
        ordinanceListHolder.violation_desc.setText(ordinanceList.getViolation_desc());
        ordinanceListHolder.violation_type.setText(ordinanceList.getViolation_type());

    }

    @NonNull
    @Override
    public OrdinanceListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ordinance_list_item,
                parent, false);
        return new OrdinanceListHolder(v);
    }

    class OrdinanceListHolder extends RecyclerView.ViewHolder{
        TextView violation_article;
        TextView violation_penalty;
        TextView violation_desc;

        TextView violation_type;


        public OrdinanceListHolder(@NonNull View itemView) {
            super(itemView);
            violation_article = itemView.findViewById(R.id.violation_article);
            violation_penalty = itemView.findViewById(R.id.violation_penalty);
            violation_desc = itemView.findViewById(R.id.ordinance_desc);

            violation_type = itemView.findViewById(R.id.violation_type);
        }
    }
}
