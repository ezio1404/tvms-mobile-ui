package com.example.tvmsph;

public class Post {
    private String post_uid;
    private String post_title;
    private String post_content;
    private String post_date;
    private String post_photoRef;
    private String post_status;
    private String agency_name;
    public Post() {
    }

    public Post(String post_uid, String post_title, String post_content, String post_date, String post_photoRef, String post_status, String agency_name) {
        this.post_uid = post_uid;
        this.post_title = post_title;
        this.post_content = post_content;
        this.post_date = post_date;
        this.post_photoRef = post_photoRef;
        this.post_status = post_status;
        this.agency_name = agency_name;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_photoRef() {
        return post_photoRef;
    }

    public void setPost_photoRef(String post_photoRef) {
        this.post_photoRef = post_photoRef;
    }

    public String getPost_status() {
        return post_status;
    }

    public void setPost_status(String post_status) {
        this.post_status = post_status;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getPost_uid() {
        return post_uid;
    }

    public void setPost_uid(String post_uid) {
        this.post_uid = post_uid;
    }
}
