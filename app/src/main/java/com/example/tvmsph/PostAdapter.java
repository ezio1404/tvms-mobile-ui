package com.example.tvmsph;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostHolder>  {
    private Context mContext;
    private List<Post> mPost;
    private OnPostListener mOnPostListener;
    public PostAdapter(Context context , List<Post> post,OnPostListener mOnPostListener){
        mContext = context;
        mPost = post;
        this.mOnPostListener = mOnPostListener;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(mContext).inflate(R.layout.post_item,parent,false);

        return new PostHolder(v,mOnPostListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PostHolder holder, int position) {
        Post postCurrent = mPost.get(position);
        holder.agency_name.setText(postCurrent.getAgency_name());
        holder.post_title.setText(postCurrent.getPost_title());
        holder.post_content.setText(postCurrent.getPost_content());
        holder.post_date.setText(postCurrent.getPost_date());

        Picasso.get()
                .load(postCurrent.getPost_photoRef())
                .placeholder(R.mipmap.ic_launcher_round)
                .fit()
                .centerCrop()
                .into(holder.post_photoRef);
    }

    @Override
    public int getItemCount() {
        return mPost.size();
    }



    public class PostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView agency_name,post_title,post_date,post_content;
        ImageView post_photoRef;
        OnPostListener onPostListener;
        public PostHolder(@NonNull View itemView,OnPostListener onPostListener) {
            super(itemView);
            agency_name = itemView.findViewById(R.id.agency_name);
            post_title = itemView.findViewById(R.id.post_title);
            post_date = itemView.findViewById(R.id.post_date);
            post_content = itemView.findViewById(R.id.post_content);
            post_photoRef= itemView.findViewById(R.id.post_photoRef);
            this.onPostListener = onPostListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onPostListener.onPostClick(getAdapterPosition());
        }
    }

    public interface OnPostListener{
        void onPostClick(int position);
    }
}
