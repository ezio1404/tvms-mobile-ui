package com.example.tvmsph;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PostDetails extends AppCompatActivity {
    private TextView post_details_title;
    private TextView post_details_agency;
    private TextView post_details_date;
    private TextView post_details_content;
    private ImageView post_details_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        getSupportActionBar().setTitle("Post Details");

        String post_uid = getIntent().getStringExtra("post_uid");
        String post_agency = getIntent().getStringExtra("post_agency");
        String post_title = getIntent().getStringExtra("post_title");
        String post_date = getIntent().getStringExtra("post_date");
        String post_content = getIntent().getStringExtra("post_content");
        String post_photoRef = getIntent().getStringExtra("post_photoRef");
        post_details_title=findViewById(R.id.post_details_title);
        post_details_agency=findViewById(R.id.post_details_agency);
        post_details_date=findViewById(R.id.post_details_date);
        post_details_content=findViewById(R.id.post_details_content);
        post_details_image=findViewById(R.id.post_details_image);

        post_details_title.setText(post_title);
        post_details_agency.setText(post_agency);
        post_details_date.setText(post_date);
        post_details_content.setText(post_content);

        Picasso.get()
                .load(post_photoRef)
                .placeholder(R.mipmap.ic_launcher_round)
                .fit()
                .centerCrop()
                .into(post_details_image);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
