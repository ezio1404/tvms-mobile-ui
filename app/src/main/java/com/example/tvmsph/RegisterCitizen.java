package com.example.tvmsph;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;

import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterCitizen extends AppCompatActivity {
    private TextInputEditText fname, lname, mi, phone, addr, city, postal, email, password;
    private Button btnRegisterCitizen;
    private ProgressBar progressBar;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference citizenRef = db.collection("citizen");
    private FirebaseAuth mAuth;
    private static final String TAG = "RegisterCitizen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_citizen);
        progressBar = findViewById(R.id.progress_barRegister);

        fname = findViewById(R.id.citizen_fname);
        lname = findViewById(R.id.citizen_lname);
        mi = findViewById(R.id.citizen_mi);

        addr = findViewById(R.id.citizen_addr);
        city = findViewById(R.id.citizen_city);
        postal = findViewById(R.id.citizen_postal);

        email = findViewById(R.id.citizen_email);
        password = findViewById(R.id.citizen_password);
        phone = findViewById(R.id.citizen_phone);

// Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();


        btnRegisterCitizen = findViewById(R.id.btnRegisterCitizen);
        btnRegisterCitizen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                btnRegisterCitizen.setVisibility(View.GONE);
                String mFname = fname.getText().toString().trim();
                String mLname = lname.getText().toString().trim();
                String mMi = mi.getText().toString().trim();

                String mAddr = addr.getText().toString().trim();
                String mCity = city.getText().toString().trim();
                String mPostal = postal.getText().toString().trim();

                String mEmail = email.getText().toString().trim();
                String mPassword = password.getText().toString().trim();
                String mPhone = phone.getText().toString().trim();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
                String format = simpleDateFormat.format(new Date());
                Citizen citizen = new Citizen(mFname, mLname, mMi, mAddr, mCity, mPostal, mEmail, mPhone, "citizen", format);
                if (mFname.isEmpty()) {
                    fname.setError("Please Enter An Firstname");
                    fname.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }
                if (mLname.isEmpty()) {
                    lname.setError("Please Enter An Lastname");
                    lname.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }

                if (mAddr.isEmpty()) {
                    addr.setError("Please Enter An Address");
                    addr.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }
                if (mCity.isEmpty()) {
                    city.setError("Please Enter a city");
                    city.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }
                if (mPostal.isEmpty()) {
                    postal.setError("Please Enter a Postal");
                    postal.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }

                if (mEmail.isEmpty()) {
                    email.setError("Please Enter An Email");
                    email.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }
                if (mPassword.isEmpty()) {
                    password.setError("Please Enter a Password");
                    password.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                }

                if (mPhone.isEmpty()) {
                    phone.setError("Please Enter a phone nubmer");
                    phone.requestFocus();
                    progressBar.setVisibility(View.GONE);
                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                } else if (mFname.isEmpty() && mLname.isEmpty() && mAddr.isEmpty() && mCity.isEmpty() && mPostal.isEmpty() && mEmail.isEmpty() && mPassword.isEmpty() && mPhone.isEmpty()) {
                    Toast.makeText(RegisterCitizen.this, "Fields are Empty", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                } else if (!mFname.isEmpty() && !mLname.isEmpty() && !mAddr.isEmpty() && !mCity.isEmpty() && !mPostal.isEmpty() && !mEmail.isEmpty() && !mPassword.isEmpty() && !mPhone.isEmpty()) {
                    mAuth.createUserWithEmailAndPassword(mEmail, mPassword)
                            .addOnCompleteListener(RegisterCitizen.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    progressBar.setVisibility(View.GONE);
                                    btnRegisterCitizen.setVisibility(View.VISIBLE);
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "createUserWithEmail:success");
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        citizenRef.document(user.getUid()).set(citizen);
                                        fname.setText(null);
                                        lname.setText(null);
                                        mi.setText(null);
                                        addr.setText(null);
                                        city.setText(null);
                                        postal.setText(null);
                                        email.setText(null);
                                        password.setText(null);
                                        phone.setText(null);
                                        startActivity(new Intent(RegisterCitizen.this, CitizenDashboard.class));
                                        Toast.makeText(RegisterCitizen.this, "Welcome " + citizen.getCitizen_fname(),
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(RegisterCitizen.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    }
                                    // ...
                                }
                            });
                }

            }
        });

    }


}
