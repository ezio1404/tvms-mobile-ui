package com.example.tvmsph;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Upload {

    private String mName;
    private String mImageUrl;

    public Upload(String name, String imageUrl) {

        if (name.trim().toString() == "") {
            name ="no name";
        }
        this.mName = name;
        this.mImageUrl = imageUrl;
    }


    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
}
