package com.example.tvmsph;

public class UserType {
    private String type;

    public UserType() {
    }

    public UserType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
