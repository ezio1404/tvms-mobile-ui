package com.example.tvmsph;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ViolationDriverAdapter extends RecyclerView.Adapter<ViolationDriverAdapter.ViolationDriverHolder> {
    private Context mContext;
    private List<ViolationItem> mViolationDriver;
    private ViolationDriverAdapter.OnViolationDriverListener onViolationDriverListener;
    public ViolationDriverAdapter(Context context, List<ViolationItem> mViolationDriver, ViolationDriverAdapter.OnViolationDriverListener onViolationDriverListener) {
        mContext = context;
        this.mViolationDriver = mViolationDriver;
        this.onViolationDriverListener = onViolationDriverListener;
    }

    @NonNull
    @Override
    public ViolationDriverHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.violation_item,
                parent, false);
        return new ViolationDriverAdapter.ViolationDriverHolder(v, onViolationDriverListener);
    }


    @Override
    public void onBindViewHolder(@NonNull ViolationDriverHolder holder, int position) {
        ViolationItem currentViolationItem = mViolationDriver.get(position);
        holder.violation_article.setText(currentViolationItem.getViolation_article());
        holder.violation_desc.setText(currentViolationItem.getViolation_desc());
        holder.ordinance_desc.setText(currentViolationItem.getOrdinance_desc());
        holder.violation_plateNo.setText(currentViolationItem.getVehicle_plateNo());
        holder.violation_paymentStatus.setText(currentViolationItem.getPayment_status());
        holder.violation_time.setText(currentViolationItem.getViolation_time());
        holder.violation_dueDate.setText(currentViolationItem.getViolation_dueDate());
        holder.violation_penalty.setText(currentViolationItem.getViolation_penalty());
        holder.violation_location.setText(currentViolationItem.getViolation_location());
        holder.violation_date.setText(currentViolationItem.getViolation_date());
//        holder.violation_dueDate.setTextColor(Color.RED);


    }

    @Override
    public int getItemCount() {
        return mViolationDriver.size();
    }

    class ViolationDriverHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView violation_article;
        TextView violation_desc;
        TextView ordinance_desc;
        TextView violation_plateNo;
        TextView violation_paymentStatus;
        TextView violation_date;
        TextView violation_time;
        TextView violation_dueDate;
        TextView violation_location;
        TextView violation_penalty;
        OnViolationDriverListener onViolationDriverListener;



        public ViolationDriverHolder(@NonNull View itemView, OnViolationDriverListener onViolationDriverListener) {
            super(itemView);

            violation_article = itemView.findViewById(R.id.violation_article);
            violation_desc = itemView.findViewById(R.id.violation_desc);
            violation_penalty= itemView.findViewById(R.id.violation_penalty);
            ordinance_desc = itemView.findViewById(R.id.ordinance_desc);
            violation_plateNo = itemView.findViewById(R.id.violation_plateNo);
            violation_paymentStatus = itemView.findViewById(R.id.violation_paymentStatus);
            violation_date = itemView.findViewById(R.id.violation_date);

            violation_time = itemView.findViewById(R.id.violation_time);
            violation_dueDate = itemView.findViewById(R.id.violation_dueDate);
            violation_location = itemView.findViewById(R.id.violation_location);
            this.onViolationDriverListener = onViolationDriverListener;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            onViolationDriverListener.onViolationDriverclick(getAdapterPosition());
        }
    }

    public interface OnViolationDriverListener {
        void onViolationDriverclick(int position);
    }

}
