package com.example.tvmsph;

public class ViolationItem {
    public String enforcer_uid;
    public String license_no;
    public String violation_uid;
    public String vehicle_plateNo;
    public String ordinance_desc;
    public String violation_penalty;

    public String violation_article;
    public String violation_desc;
    public String violation_dueDate;
    public String violation_location;
    public String violation_time;
    public String violation_date;
    public String payment_status;
    public String violation_form_uid;

    public ViolationItem() {
    }


    public ViolationItem(String enforcer_uid, String license_no, String violation_uid, String vehicle_plateNo, String violation_desc, String violation_dueDate, String violation_location, String violation_time, String violation_date, String payment_status) {
        this.enforcer_uid = enforcer_uid;
        this.license_no = license_no;
        this.violation_uid = violation_uid;
        this.vehicle_plateNo = vehicle_plateNo;
        this.violation_desc = violation_desc;
        this.violation_dueDate = violation_dueDate;
        this.violation_location = violation_location;
        this.violation_time = violation_time;
        this.violation_date = violation_date;
        this.payment_status = payment_status;
    }

    public String getEnforcer_uid() {
        return enforcer_uid;
    }

    public void setEnforcer_uid(String enforcer_uid) {
        this.enforcer_uid = enforcer_uid;
    }

    public String getLicense_no() {
        return license_no;
    }

    public void setLicense_no(String license_no) {
        this.license_no = license_no;
    }

    public String getViolation_uid() {
        return violation_uid;
    }

    public void setViolation_uid(String violation_uid) {
        this.violation_uid = violation_uid;
    }

    public String getVehicle_plateNo() {
        return vehicle_plateNo;
    }

    public void setVehicle_plateNo(String vehicle_plateNo) {
        this.vehicle_plateNo = vehicle_plateNo;
    }

    public String getViolation_desc() {
        return violation_desc;
    }

    public void setViolation_desc(String violation_desc) {
        this.violation_desc = violation_desc;
    }

    public String getViolation_dueDate() {
        return violation_dueDate;
    }

    public void setViolation_dueDate(String violation_dueDate) {
        this.violation_dueDate = violation_dueDate;
    }

    public String getViolation_location() {
        return violation_location;
    }

    public void setViolation_location(String violation_location) {
        this.violation_location = violation_location;
    }

    public String getOrdinance_desc() {
        return ordinance_desc;
    }

    public void setOrdinance_desc(String ordinance_desc) {
        this.ordinance_desc = ordinance_desc;
    }

    public String getViolation_article() {
        return violation_article;
    }

    public void setViolation_article(String violation_article) {
        this.violation_article = violation_article;
    }

    public String getViolation_penalty() {
        return violation_penalty;
    }

    public void setViolation_penalty(String violation_penalty) {
        this.violation_penalty = violation_penalty;
    }

    public String getViolation_time() {
        return violation_time;
    }

    public void setViolation_time(String violation_time) {
        this.violation_time = violation_time;
    }

    public String getViolation_date() {
        return violation_date;
    }

    public void setViolation_date(String violation_date) {
        this.violation_date = violation_date;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getViolation_form_uid() {
        return violation_form_uid;
    }

    public void setViolation_form_uid(String violation_form_uid) {
        this.violation_form_uid = violation_form_uid;
    }
}
