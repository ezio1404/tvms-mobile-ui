package com.example.tvmsph;

public class ViolatorOrdinance {

    private String violation_article;
    private String status;
    private String violation_desc;
    private String violation_penalty;
    private String violation_type;
    private String violation_uid;
    private boolean isSelected;
    public ViolatorOrdinance() {
    }

    public ViolatorOrdinance(String violation_article, String violation_desc, String violation_penalty, String violation_type, String status) {
        this.violation_article = violation_article;
        this.violation_desc = violation_desc;
        this.violation_penalty = violation_penalty;
        this.violation_type = violation_type;
        this.status = status;
    }

    public String getViolation_article() {
        return violation_article;
    }

    public void setViolation_article(String violation_article) {
        this.violation_article = violation_article;
    }


    public String getViolation_desc() {
        return violation_desc;
    }

    public void setViolation_desc(String violation_desc) {
        this.violation_desc = violation_desc;
    }

    public String getViolation_penalty() {
        return violation_penalty;
    }

    public void setViolation_penalty(String violation_penalty) {
        this.violation_penalty = violation_penalty;
    }

    public String getViolation_type() {
        return violation_type;
    }

    public void setViolation_type(String violation_type) {
        this.violation_type = violation_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getViolation_uid() {
        return violation_uid;
    }

    public void setViolation_uid(String violation_uid) {
        this.violation_uid = violation_uid;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
