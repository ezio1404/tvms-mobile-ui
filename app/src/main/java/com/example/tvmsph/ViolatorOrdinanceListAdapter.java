package com.example.tvmsph;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViolatorOrdinanceListAdapter extends RecyclerView.Adapter<ViolatorOrdinanceListAdapter.ViolatorHolder> {
    private Context mContext;
    private List<ViolatorOrdinance> mViolationOrdinance;
    private OnViolationListener onViolationListener;
    private ArrayList<ViolatorOrdinance> mListCheckeditems = new ArrayList<>();

    public ViolatorOrdinanceListAdapter(Context context, List<ViolatorOrdinance> ViolationOrdinance, OnViolationListener monViolationListener) {
        mContext = context;
        mViolationOrdinance = ViolationOrdinance;
        this.onViolationListener = monViolationListener;
    }

    public ViolatorOrdinanceListAdapter(Context context, List<ViolatorOrdinance> ViolationOrdinance) {
        mContext = context;
        mViolationOrdinance = ViolationOrdinance;

    }

    @NonNull
    @Override
    public ViolatorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.violator_ordinance, parent, false);

        return new ViolatorOrdinanceListAdapter.ViolatorHolder(v, onViolationListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViolatorHolder holder, int position) {
        ViolatorOrdinance violationCurrent = mViolationOrdinance.get(position);
        holder.violator_article.setText(violationCurrent.getViolation_article());
        holder.violator_desc.setText(violationCurrent.getViolation_desc());
        holder.violator_penalty.setText(violationCurrent.getViolation_penalty()+".00");
        holder.checkBox.setChecked(violationCurrent.isSelected());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                ViolatorOrdinance CurrentViolatorOrdinance = (ViolatorOrdinance) mViolationOrdinance.get(position);
                if (b) {
                    CurrentViolatorOrdinance.setSelected(true);
                    mListCheckeditems.add(CurrentViolatorOrdinance);
                    Toast.makeText(mContext, "Added :" + CurrentViolatorOrdinance.getViolation_desc(), Toast.LENGTH_SHORT).show();
                    if (user != null) {
                        db.collection("form").document(user.getUid()).collection("violation").document(violationCurrent.getViolation_uid()).set(violationCurrent);
                    }
                } else {
                    CurrentViolatorOrdinance.setSelected(false);
                    mListCheckeditems.remove(CurrentViolatorOrdinance);
                    Toast.makeText(mContext, "removed" + CurrentViolatorOrdinance.getViolation_desc(), Toast.LENGTH_SHORT).show();
                    if (user != null) {
                        db.collection("form").document(user.getUid()).collection("violation").document(violationCurrent.getViolation_uid()).delete();
                    }

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mViolationOrdinance.size();
    }

    public int getCheckeditemsCount() {
        return mListCheckeditems.size();
    }

    public static class ViolatorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView violator_article, violator_desc, violator_penalty;
        CheckBox checkBox;
        OnViolationListener onViolationListener;

        public ViolatorHolder(@NonNull View itemView, ViolatorOrdinanceListAdapter.OnViolationListener onViolationListener) {
            super(itemView);
            violator_article = itemView.findViewById(R.id.violation_article_violator);
            violator_desc = itemView.findViewById(R.id.violation_desc_violator);
            violator_penalty = itemView.findViewById(R.id.violation_penalty_violator);
            checkBox = itemView.findViewById(R.id.checkboxViolation);
            this.onViolationListener = onViolationListener;
            itemView.setOnClickListener(this);
        }

        public void setOnViolationListener(OnViolationListener ic) {
            this.onViolationListener = ic;
        }

        @Override
        public void onClick(View view) {
            onViolationListener.onViolationClick(view, getLayoutPosition());
        }
    }

    public interface OnViolationListener {
        void onViolationClick(View view, int position);
    }
}
