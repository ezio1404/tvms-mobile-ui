package com.example.tvmsph;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.se.omapi.Channel;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.example.tvmsph.App.CHANNEL_1_ID;

public class dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PostAdapter.OnPostListener {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private RecyclerView mRecyclerView;
    private PostAdapter mAdapter;
    private List<Post> mPost;
    private NotificationManagerCompat notificationManagerCompat;

    private TextView nameUser, emailUser;
    private ImageView imgUser;

    private ProgressBar mProgresbar;
    private static final String TAG = "dashboard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enforcer_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mProgresbar = findViewById(R.id.progress_bar_enforcer_post);

        mRecyclerView = findViewById(R.id.recycler_view_post_enforcer);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mPost = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();


//      Drawer start
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.nav_home).setVisible(true);
        menu.findItem(R.id.nav_inquiry).setVisible(true);
        menu.findItem(R.id.nav_violators).setVisible(true);
        menu.findItem(R.id.nav_violation).setVisible(true);
        menu.findItem(R.id.nav_ra).setVisible(true);
        menu.findItem(R.id.nav_vp).setVisible(true);
        menu.findItem(R.id.nav_help).setVisible(true);
        menu.findItem(R.id.nav_profile).setVisible(true);
        menu.findItem(R.id.nav_logout).setVisible(true);
        View headerLayout = navigationView.getHeaderView(0);
        nameUser = headerLayout.findViewById(R.id.mDashboardName);
        emailUser = headerLayout.findViewById(R.id.mDashboardEmail);
        imgUser = headerLayout.findViewById(R.id.imageView);
        notificationManagerCompat = NotificationManagerCompat.from(this);
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                getPost();
                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> agency = new ArrayList<>();

                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agency.add(doc.getString("agency_name"));

                                agencyRef.document(doc.getId()).collection("enforcer")
                                        .whereEqualTo("status", "enforcer")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @SuppressLint("SetTextI18n")
                                            @Override
                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    System.err.println("Listen failed:" + e);
                                                    return;
                                                }
                                                List<String> enforcerList = new ArrayList<>();
                                                for (DocumentSnapshot doc : snapshots) {
                                                    Enforcer enforcer = doc.toObject(Enforcer.class);
                                                    if (doc.get("enforcer_lname") != null) {
                                                        enforcerList.add(doc.getId());
                                                        if (doc.getId().equals(user.getUid())) {
                                                            getSupportActionBar().setTitle("Home / Enforcer");
                                                            nameUser.setText(doc.getString("enforcer_lname") + "," + doc.getString("enforcer_fname"));
                                                            emailUser.setText(doc.getString("enforcer_email"));
                                                            Picasso.get()
                                                                    .load(doc.getString("enforcer_photoRef"))
                                                                    .placeholder(R.mipmap.ic_launcher_round)
                                                                    .fit()
                                                                    .centerCrop()
                                                                    .into(imgUser);
                                                            menu.findItem(R.id.nav_home).setVisible(true);
                                                            menu.findItem(R.id.nav_inquiry).setVisible(true);
                                                            menu.findItem(R.id.nav_violators).setVisible(true);
                                                            menu.findItem(R.id.nav_violation).setVisible(false);
                                                            menu.findItem(R.id.nav_ra).setVisible(true);
                                                            menu.findItem(R.id.nav_vp).setVisible(true);
                                                            menu.findItem(R.id.nav_help).setVisible(true);
                                                            menu.findItem(R.id.nav_profile).setVisible(true);
                                                            menu.findItem(R.id.nav_logout).setVisible(true);
                                                        }
                                                    }
                                                }

                                                System.out.println("Current enforcer active: " + enforcerList);
                                            }
                                        });

                            }
                        }
                        System.out.println("Current Agency active: " + agency);
                    }
                });
                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> agency = new ArrayList<>();

                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agency.add(doc.getString("agency_name"));

                                agencyRef.document(doc.getId()).collection("driver")
                                        .whereEqualTo("status", "driver")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @SuppressLint("SetTextI18n")
                                            @Override
                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    System.err.println("Listen failed:" + e);
                                                    return;
                                                }
                                                List<String> driverlist = new ArrayList<>();
                                                for (DocumentSnapshot doc : snapshots) {
                                                    Driver driver = doc.toObject(Driver.class);
                                                    if (doc.get("driver_lname") != null) {
                                                        driverlist.add(doc.getId());
                                                        if (doc.getId().equals(user.getUid())) {
                                                            getSupportActionBar().setTitle("Home / Driver");
                                                            nameUser.setText(doc.getString("driver_lname") + "," + doc.getString("driver_fname"));
                                                            emailUser.setText(doc.getString("driver_email"));
                                                            Picasso.get()
                                                                    .load(doc.getString("enforcer_photoRef"))
                                                                    .placeholder(R.mipmap.ic_launcher_round)
                                                                    .fit()
                                                                    .centerCrop()
                                                                    .into(imgUser);
                                                            menu.findItem(R.id.nav_home).setVisible(true);
                                                            menu.findItem(R.id.nav_inquiry).setVisible(true);
                                                            menu.findItem(R.id.nav_violators).setVisible(false);
                                                            menu.findItem(R.id.nav_violation).setVisible(true);
                                                            menu.findItem(R.id.nav_ra).setVisible(true);
                                                            menu.findItem(R.id.nav_vp).setVisible(true);
                                                            menu.findItem(R.id.nav_help).setVisible(true);
                                                            menu.findItem(R.id.nav_profile).setVisible(true);
                                                            menu.findItem(R.id.nav_logout).setVisible(true);
                                                            agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                @Override
                                                                public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                    if (e != null) {
                                                                        System.err.println("Listen failed:" + e);
                                                                        return;
                                                                    }
                                                                    List<String> agency = new ArrayList<>();
                                                                    for (DocumentSnapshot doc : snapshots) {
                                                                        if (doc.get("agency_name") != null) {
                                                                            agencyRef.document(doc.getId()).collection("violation_forms").orderBy("violation_dateCreated").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                                @Override
                                                                                public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                                    if (e != null) {
                                                                                        System.err.println("Listen failed:" + e);
                                                                                        return;
                                                                                    }
                                                                                    List<String> violationFormList = new ArrayList<>();
                                                                                    for (DocumentSnapshot violationFormDoc : snapshots) {
                                                                                        if (violationFormDoc.get("violation_desc") != null) {
                                                                                            ////////////////////////
                                                                                            //violation
                                                                                            agencyRef.document(doc.getId()).collection("ordinance").whereEqualTo("status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                                                @Override
                                                                                                public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                                                    if (e != null) {
                                                                                                        System.err.println("Listen failed:" + e);
                                                                                                        return;
                                                                                                    }
                                                                                                    List<String> ordinanceList = new ArrayList<>();
                                                                                                    for (DocumentSnapshot ordinanceDoc : snapshots) {
                                                                                                        if (ordinanceDoc.get("violation_article") != null) {
                                                                                                            ordinanceList.add(ordinanceDoc.getId());
                                                                                                            String formOrdinanceId = violationFormDoc.getString("violation_uid");
                                                                                                            if (formOrdinanceId.equals(ordinanceDoc.getId())) {
                                                                                                                //////////////////////////////////////////
                                                                                                                //driver
                                                                                                                agencyRef.document(doc.getId()).collection("driver").whereEqualTo("status", "driver").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                                                                    @Override
                                                                                                                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                                                                        mProgresbar.setVisibility(View.GONE);
                                                                                                                        if (e != null) {
                                                                                                                            System.err.println("Listen failed:" + e);
                                                                                                                            return;
                                                                                                                        }
                                                                                                                        List<String> driverList = new ArrayList<>();
                                                                                                                        for (DocumentSnapshot driverDoc : snapshots) {
                                                                                                                            Driver driver = driverDoc.toObject(Driver.class);
                                                                                                                            if (driverDoc.get("license_no") != null) {
                                                                                                                                driverList.add(driverDoc.getId());
                                                                                                                                String formLicense = violationFormDoc.getString("license_no");
                                                                                                                                if (formLicense.equals(driver.getLicense_no()) && driverDoc.getId().equals(user.getUid()) ) {
                                                                                                                                    String details = ordinanceDoc.getString("violation_article") + " - " + ordinanceDoc.getString("violation_desc");
                                                                                                                                    violationFormList.add(violationFormDoc.getId());
                                                                                                                                    String title, message;
                                                                                                                                    title = "Violation Added";
                                                                                                                                    message = "You have Violated " + details;
                                                                                                                                    getNotification(title, message);
                                                                                                                                }
                                                                                                                            }

                                                                                                                        }
                                                                                                                        System.out.println("driverList " + driverList);
                                                                                                                    }
                                                                                                                });
                                                                                                                //end driver
                                                                                                                /////////////////////////
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    System.out.println("ordinanceList " + ordinanceList);
                                                                                                }
                                                                                            });
                                                                                            //end violation
                                                                                            /////////////////////////


                                                                                        }


                                                                                    }

                                                                                }
                                                                            });


                                                                        }
                                                                    }
                                                                    System.out.println("Current Agency active: " + agency);
                                                                }
                                                            });

                                                        }
                                                    }
                                                }

                                                System.out.println("Current Driver active: " + driverlist);
                                            }
                                        });

                            }
                        }
                        System.out.println("Current Agency active: " + agency);
                    }
                });
                db.collection("citizen").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> citizenList = new ArrayList<>();

                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("citizen_fname") != null) {
                                citizenList.add(doc.getId());
                                if (doc.getId().equals(user.getUid())) {
                                    getSupportActionBar().setTitle("Home / Citizen");
                                    nameUser.setText(doc.getString("citizen_lname") + "," + doc.getString("citizen_fname"));
                                    emailUser.setText(doc.getString("citizen_email"));
//                                    Picasso.get()
//                                            .load(doc.getString("enforcer_photoRef"))
//                                            .placeholder(R.mipmap.ic_launcher_round)
//                                            .fit()
//                                            .centerCrop()
//                                            .into(imgUser);
                                    menu.findItem(R.id.nav_home).setVisible(true);
                                    menu.findItem(R.id.nav_inquiry).setVisible(true);
                                    menu.findItem(R.id.nav_violators).setVisible(false);
                                    menu.findItem(R.id.nav_violation).setVisible(false);
                                    menu.findItem(R.id.nav_ra).setVisible(true);
                                    menu.findItem(R.id.nav_vp).setVisible(true);
                                    menu.findItem(R.id.nav_help).setVisible(true);
                                    menu.findItem(R.id.nav_profile).setVisible(false);
                                    menu.findItem(R.id.nav_logout).setVisible(true);
                                }
                            }
                        }
                        System.out.println("Current Citizen active: " + citizenList);
                    }
                });

//////////////notif


            } else {
                startActivity(new Intent(dashboard.this, LoginActivity.class));
                Toast.makeText(this, "Please Login", Toast.LENGTH_SHORT).show();
            }
        };


    }

    private void getNotification(String title, String message) {
        Log.d(TAG, "getNotification: " + title + "---" + message);
        Intent activityIntent = new Intent(this, violation.class);
        PendingIntent contentIntet = PendingIntent.getActivity(this, 0, activityIntent, PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.tvms_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(contentIntet)
                .setOnlyAlertOnce(true)
                .build();
        notificationManagerCompat.notify(1, notification);
    }


    private void getPost() {
        agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }
                List<String> agency = new ArrayList<>();
                for (DocumentSnapshot doc : snapshots) {
                    if (doc.get("agency_name") != null) {
                        agency.add(doc.getString("agency_name"));
                        agencyRef.document(doc.getId()).collection("post")
                                .whereEqualTo("post_status", "active")
                                .orderBy("post_date", Query.Direction.DESCENDING)
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                        mProgresbar.setVisibility(View.GONE);
                                        if (e != null) {
                                            System.err.println("Listen failed:" + e);
                                            return;
                                        }
                                        List<String> postList = new ArrayList<>();
                                        for (DocumentChange docPost : snapshots.getDocumentChanges()) {
                                            Post post = docPost.getDocument().toObject(Post.class);
                                            switch (docPost.getType()) {
                                                case ADDED:
                                                    System.out.println("New post: " + docPost.getDocument().getData());
                                                    postList.add(docPost.getDocument().getId());
                                                    post.setAgency_name(doc.getString("agency_name"));
                                                    post.setPost_uid(docPost.getDocument().getId());
                                                    mPost.add(post);

                                                    break;
                                                case MODIFIED:
                                                    System.out.println("Modified post: " + docPost.getDocument().getData());


                                                    break;
                                                case REMOVED:
                                                    System.out.println("Removed post: " + docPost.getDocument().getData());
                                                    post.setPost_uid(docPost.getDocument().getId());
                                                    mPost.remove(post);


                                                    break;
                                                default:
                                                    break;
                                            }
                                        }

                                        mAdapter = new PostAdapter(dashboard.this, mPost, dashboard.this);
                                        mRecyclerView.setAdapter(mAdapter);
                                        System.out.println("Agency : " + doc.getString("agency_name") + "Current post active: " + postList);
                                    }
                                });
                    }
                }
                System.out.println("Current Agency active: " + agency);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_violators) {
            Intent violator = new Intent(this, AddViolationActivity.class);
            startActivity(violator);
            finish();
        } else if (id == R.id.nav_inquiry) {
            startActivity(new Intent(this, inquiry.class));
            finish();
        } else if (id == R.id.nav_violation) {
            startActivity(new Intent(this, violation.class));
            finish();
        } else if (id == R.id.nav_vp) {
            startActivity(new Intent(this, ordinance.class));
            finish();
        } else if (id == R.id.nav_ra) {
            startActivity(new Intent(this, laws.class));
            finish();
        } else if (id == R.id.nav_help) {
            startActivity(new Intent(this, help.class));
            finish();
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(this, profile.class));
            finish();
        } else if (id == R.id.nav_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(dashboard.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPost.clear();

    }

    @Override
    public void finish() {
        super.finish();
        mPost.clear();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onPostClick(int position) {
        Intent postDetail = new Intent(dashboard.this, PostDetails.class);
        postDetail.putExtra("post_uid", mPost.get(position).getPost_uid());
        postDetail.putExtra("post_agency", mPost.get(position).getAgency_name());
        postDetail.putExtra("post_content", mPost.get(position).getPost_content());
        postDetail.putExtra("post_date", mPost.get(position).getPost_date());
        postDetail.putExtra("post_title", mPost.get(position).getPost_title());
        postDetail.putExtra("post_photoRef", mPost.get(position).getPost_photoRef());

        startActivity(postDetail);
        Toast.makeText(this, "Title :" + mPost.get(position).getPost_title(), Toast.LENGTH_SHORT).show();
    }
}
