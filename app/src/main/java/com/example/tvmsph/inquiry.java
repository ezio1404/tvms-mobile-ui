package com.example.tvmsph;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class inquiry extends AppCompatActivity {
    Button btnLTOservices;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry);
        getSupportActionBar().setTitle("Inquiry");
        btnLTOservices = findViewById(R.id.btnLTOservices);
        btnLTOservices.setOnClickListener(view->{
            startActivity(new Intent(inquiry.this,LTO_services.class));
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
