package com.example.tvmsph;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ordinance extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");


    private OrdinanceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordinance);
        getSupportActionBar().setTitle("Ordinance");

        setUpRecyclerView();


    }

    private void setUpRecyclerView() {
        Query query = agencyRef.orderBy("agency_name", Query.Direction.ASCENDING).whereEqualTo("subscription_status","active");
        FirestoreRecyclerOptions<ordinance_agency> options = new FirestoreRecyclerOptions.Builder<ordinance_agency>()
                .setQuery(query, ordinance_agency.class)
                .build();


        adapter = new OrdinanceAdapter(options);
        RecyclerView recyclerView= findViewById(R.id.recycler_view_ordinance);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new OrdinanceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                ordinance_agency ordinance_agency = documentSnapshot.toObject(ordinance_agency.class);
                String id=documentSnapshot.getId();
                String agency_name = ordinance_agency.getAgency_name();
                Toast.makeText(ordinance.this,"Agency Name " + agency_name ,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ordinance.this, ordinance_list.class);
                // agency ordinace list
                intent.putExtra("agency_uid", id);
                intent.putExtra("agency_name", agency_name);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}
