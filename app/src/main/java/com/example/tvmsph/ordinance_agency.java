package com.example.tvmsph;

public class ordinance_agency {

    private String agency_name;
    private String agency_head;

    public  ordinance_agency(){}
    public  ordinance_agency(String agency_name, String agency_head){
        this.agency_name = agency_name;
        this.agency_head = agency_head;
    }


    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getAgency_head() {
        return agency_head;
    }

    public void setAgency_head(String agency_head) {
        this.agency_head = agency_head;
    }
}
