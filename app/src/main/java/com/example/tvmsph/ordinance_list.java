package com.example.tvmsph;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ordinance_list extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference ordinanceListRef ;


    private OrdinanceListAdapter adapter;

    TextView ordinance_list_agency_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordinance_list);
        ordinance_list_agency_name =  findViewById(R.id.ordinance_list_agency_name);
        String mAgency_name = getIntent().getStringExtra("agency_name");
        String mAgency_uid = getIntent().getStringExtra("agency_uid");
        ordinance_list_agency_name.setText(mAgency_name);
        getSupportActionBar().setTitle("Ordinance / "+mAgency_name);

//        ordinanceListRef =db.collection("agency").document(mAgency_uid).collection("ordinance");

        ordinanceListRef =db.collection("agency/"+mAgency_uid+"/ordinance");


        setUpRecyclerView();

    }
    private void setUpRecyclerView() {
        Query query = ordinanceListRef.orderBy("violation_type", Query.Direction.ASCENDING).whereEqualTo("status","active");
        FirestoreRecyclerOptions<OrdinanceList> options = new FirestoreRecyclerOptions.Builder<OrdinanceList>()
                .setQuery(query, OrdinanceList.class)
                .build();

        adapter = new OrdinanceListAdapter(options);
        RecyclerView recyclerView= findViewById(R.id.recycler_view_ordinance_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }
    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
