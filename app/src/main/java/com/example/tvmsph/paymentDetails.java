package com.example.tvmsph;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class paymentDetails extends AppCompatActivity {

    TextView txtId, txtAmount, txtStatus;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG = "paymentDetails";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment_details);

        txtId = (TextView) findViewById(R.id.txtID);
        txtAmount = (TextView) findViewById(R.id.txtAmount);
        txtStatus = (TextView) findViewById(R.id.txtStatus);

        Intent intent = getIntent();
        String agencyUid = intent.getStringExtra("agencyDocUid");
        String formUid = intent.getStringExtra("violationFormUid");
        String violationFormPenalty= intent.getStringExtra("violation_penalty");
        Log.d(TAG, "onCreate: "+ intent.getStringExtra("PaymentAmount"));
        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("paymentDetails"));
//            showDetails(jsonObject.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
            Map<String, Object> data = new HashMap<>();
            data.put("payment_status", "paid");
            if (jsonObject.getJSONObject("response").getString("state").equals("approved")) {
                agencyRef.document(agencyUid).collection("violation_forms").document(formUid).update(data);
                Map<String, Object> paymentData = new HashMap<>();
                paymentData.put("driver_uid", intent.getStringExtra("driver_uid"));
                paymentData.put("payment",violationFormPenalty);
                paymentData.put("payment_id", jsonObject.getJSONObject("response").getString("id"));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
                String currentDateandTime = sdf.format(new Date());
                paymentData.put("payment_dateCreated",currentDateandTime);
                agencyRef.document(agencyUid).collection("payment_logs").add(paymentData);
                Toast.makeText(paymentDetails.this, "Payment Success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(paymentDetails.this, violation.class));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private void showDetails(JSONObject response, String paymentAmount) {
//        try {
//            txtId.setText(response.getString("id"));
//            txtStatus.setText(response.getString("state"));
//            txtAmount.setText(response.getString(String.format("$%s", paymentAmount)));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
}
