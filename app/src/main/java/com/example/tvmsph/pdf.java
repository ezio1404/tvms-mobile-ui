package com.example.tvmsph;

import android.os.Bundle;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.github.barteksc.pdfviewer.PDFView;

public class pdf extends AppCompatActivity {

    PDFView pdf;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdf);
        pdf=findViewById(R.id.pdf1);
        String pdfName = getIntent().getStringExtra("pdfName");
        pdf.fromAsset(pdfName).load();
    }
}
