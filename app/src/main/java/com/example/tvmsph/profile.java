package com.example.tvmsph;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.tvmsph.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.ArrayList;
import java.util.List;

public class profile extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");

    private FirebaseAuth.AuthStateListener mAuthListener;
    private Button changeDpBtn, edit, save;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private TextInputEditText fname, lname, mi, addr, city, postal;
    private ImageView qrcodeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setTitle("Profile");
        progressBar = findViewById(R.id.progressBarProfileDriver);
        mAuth = FirebaseAuth.getInstance();


        edit = findViewById(R.id.edit);
        save = findViewById(R.id.save);

        qrcodeImage = findViewById(R.id.qrcodeImage);

        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        mi = findViewById(R.id.mi);

        addr = findViewById(R.id.addr);
        city = findViewById(R.id.city);
        postal = findViewById(R.id.postal);
        // qr code generator
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {

            }
        };
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fname.setEnabled(true);
                lname.setEnabled(true);
                mi.setEnabled(true);

                addr.setEnabled(true);
                city.setEnabled(true);
                postal.setEnabled(true);

                save.setVisibility(View.VISIBLE);
                edit.setVisibility(View.GONE);

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fname.setEnabled(false);
                        lname.setEnabled(false);
                        mi.setEnabled(false);

                        addr.setEnabled(false);
                        city.setEnabled(false);
                        postal.setEnabled(false);
                        save.setVisibility(View.GONE);
                        edit.setVisibility(View.VISIBLE);
                        Toast.makeText(profile.this, "Saved", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });


        mAuthListener = firebaseAuth ->

        {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> agency = new ArrayList<>();

                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agency.add(doc.getString("agency_name"));

                                agencyRef.document(doc.getId()).collection("driver")
                                        .whereEqualTo("status", "driver")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @Override
                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    System.err.println("Listen failed:" + e);
                                                    return;
                                                }
                                                List<String> driverlist = new ArrayList<>();
                                                for (DocumentSnapshot driverDoc : snapshots) {
                                                    Driver driver = driverDoc.toObject(Driver.class);
                                                    if (driverDoc.get("driver_lname") != null) {
                                                        driverlist.add(driverDoc.getId());
                                                        if (driverDoc.getId().equals(user.getUid())) {
                                                            getSupportActionBar().setTitle("Profile / Driver");
                                                            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                                                            try {
                                                                BitMatrix bitMatrix = multiFormatWriter.encode(driver.getLicense_no(), BarcodeFormat.QR_CODE, 200, 200);
                                                                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                                                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                                                qrcodeImage.setImageBitmap(bitmap);
                                                            } catch (WriterException error) {
                                                                error.printStackTrace();
                                                            }
                                                            System.out.println("Current Driver fname: " + driverDoc.getString("driver_fname"));
                                                            fname.setText(driver.getDriver_fname());
                                                            lname.setText(driver.getDriver_lname());
                                                            mi.setText(driver.getDriver_mi());

                                                            addr.setText(driver.getDriver_addr());
                                                            city.setText(driver.getDriver_city());
                                                            postal.setText(driver.getDriver_postal());
                                                        }
                                                    }
                                                }

                                                System.out.println("Current Driver active: " + driverlist);
                                            }
                                        });

                            }
                        }
                        System.out.println("Current Agency active: " + agency);
                    }
                });
                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> agency = new ArrayList<>();

                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agency.add(doc.getString("agency_name"));

                                agencyRef.document(doc.getId()).collection("enforcer")
                                        .whereEqualTo("status", "enforcer")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @Override
                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    System.err.println("Listen failed:" + e);
                                                    return;
                                                }
                                                List<String> enforcerList = new ArrayList<>();
                                                for (DocumentSnapshot driverDoc : snapshots) {
                                                    Enforcer enforcer = driverDoc.toObject(Enforcer.class);
                                                    if (driverDoc.get("enforcer_lname") != null) {
                                                        enforcerList.add(driverDoc.getId());
                                                        if (driverDoc.getId().equals(user.getUid())) {
                                                            getSupportActionBar().setTitle("Profile / Enforcer");
                                                            qrcodeImage.setVisibility(View.GONE);
                                                            fname.setText(enforcer.getEnforcer_fname());
                                                            lname.setText(enforcer.getEnforcer_lname());
                                                            mi.setText(enforcer.getEnforcer_mi());
                                                            addr.setText(enforcer.getEnforcer_addr());
                                                            city.setText(enforcer.getEnforcer_city());
                                                            postal.setText(enforcer.getEnforcer_postal());
                                                        }
                                                    }
                                                }

                                                System.out.println("Current enforcer active: " + enforcerList);
                                            }
                                        });

                            }
                        }
                        System.out.println("Current Agency active: " + agency);
                    }
                });
                db.collection("citizen").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        List<String> citizenList = new ArrayList<>();
                        for (DocumentSnapshot CitizenDoc : snapshots) {
                            Citizen citizen = CitizenDoc.toObject(Citizen.class);
                            if (CitizenDoc.get("citizen_fname") != null) {
                                citizenList.add(CitizenDoc.getId());
                                if (CitizenDoc.getId().equals(user.getUid())) {
                                    getSupportActionBar().setTitle("Home / Citizen");
                                    qrcodeImage.setVisibility(View.GONE);
                                    fname.setText(citizen.getCitizen_fname());
                                    lname.setText(citizen.getCitizen_lname());
                                    mi.setText(citizen.getCitizen_mi());
                                    addr.setText(citizen.getCitizen_addr());
                                    city.setText(citizen.getCitizen_city());
                                    postal.setText(citizen.getCitizen_postal());
                                }
                            }
                        }
                        System.out.println("Current Citizen active: " + citizenList);
                    }
                });
            } else {

            }
        }

        ;

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}