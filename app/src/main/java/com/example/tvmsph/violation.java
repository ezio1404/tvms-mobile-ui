package com.example.tvmsph;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class violation extends AppCompatActivity implements ViolationDriverAdapter.OnViolationDriverListener {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private RecyclerView mRecyclerView;
    private ViolationDriverAdapter mAdapter;
    private List<ViolationItem> mViolationDriverList;
    private ProgressBar mProgresbar;
    private static final String TAG = "violation";
    private String agencyDocUid, violationFormUid, user_uid,violationFormPenalty;
    public static final int PAYPAL_REQUEST_CODE = 7171;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId("AR2W7vYUKaCLGORkiZKcpl3J9A9u4w7sJobOFdgABictfda7YyZgZOAqtrYmbOefo01Wo7QoPe8L3Vkm");
    public static double totalAmount = 0;

    @Override
    protected void onDestroy() {
        stopService(new Intent(violation.this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_violation);
        getSupportActionBar().setTitle("Monitor Violation");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mProgresbar = findViewById(R.id.progress_bar_violation_driver);
        mRecyclerView = findViewById(R.id.recycler_view_violation_driver);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(violation.this));

        Intent intent = new Intent(violation.this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        mViolationDriverList = new ArrayList<>();
        mProgresbar.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                user_uid = user.getUid();
                agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        for (DocumentSnapshot doc : snapshots) {
                            if (doc.get("agency_name") != null) {
                                agencyRef.document(doc.getId()).collection("driver").whereEqualTo("status", "driver").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                        if (e != null) {
                                            System.err.println("Listen failed:" + e);
                                            return;
                                        }
                                        for (DocumentSnapshot driverDoc : snapshots) {
                                            Driver driver = driverDoc.toObject(Driver.class);
                                            if (driverDoc.get("driver_lname") != null) {
//                                                Log.d(TAG, "onEvent: " + driver.getLicense_no());
                                                agencyDocUid = doc.getId();
                                                agencyRef.document(doc.getId()).collection("ordinance").whereEqualTo("status", "active")
                                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                            @Override
                                                            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                if (e != null) {
                                                                    System.err.println("Listen failed:" + e);
                                                                    return;
                                                                }
                                                                List<String> ordinanceList = new ArrayList<>();
                                                                for (DocumentSnapshot ordinanceDoc : snapshots) {
                                                                    OrdinanceList ordinance = ordinanceDoc.toObject(OrdinanceList.class);
                                                                    if (ordinanceDoc.get("violation_article") != null) {
                                                                        ordinanceList.add(ordinance.getViolation_article());
                                                                        agencyRef.document(doc.getId()).collection("violation_forms")

                                                                                .whereEqualTo("violation_uid", ordinanceDoc.getId())
                                                                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                                                    @Override
                                                                                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                                                        if (e != null) {
                                                                                            System.err.println("Listen failed:" + e);
                                                                                            return;
                                                                                        }
                                                                                        List<String> violationFormList = new ArrayList<>();
                                                                                        for (DocumentSnapshot violationFormDoc : snapshots) {
                                                                                            ViolationItem violationItem = violationFormDoc.toObject(ViolationItem.class);
                                                                                            if (violationItem.getLicense_no() != null) {
                                                                                                if (violationItem.getLicense_no().equals(driver.getLicense_no()) && driverDoc.getId().equals(user_uid)) {
                                                                                                    Log.d(TAG, "onEvent: eualkq" + violationItem.getLicense_no().equals(driver.getLicense_no()));
                                                                                                    violationFormList.add(violationFormDoc.getId());
                                                                                                    violationItem.setViolation_article(ordinanceDoc.getString("violation_article"));
                                                                                                    violationItem.setViolation_penalty(ordinanceDoc.getString("violation_penalty"));
                                                                                                    violationItem.setOrdinance_desc(ordinanceDoc.getString("violation_desc"));
                                                                                                    violationItem.setViolation_form_uid(violationFormDoc.getId());
                                                                                                    mViolationDriverList.add(violationItem);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        Log.d(TAG, "onEvent: violationFormList" + violationFormList);
                                                                                        mAdapter = new ViolationDriverAdapter(violation.this, mViolationDriverList, violation.this);
                                                                                        mRecyclerView.setAdapter(mAdapter);
                                                                                    }
                                                                                });
                                                                    }
                                                                }
//                                                                Log.d(TAG, "onEvent: list ordinance" + ordinanceList);

                                                            }
                                                        });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                });


            } else {

            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        // Check if user is signed in (non-null) and update UI accordingly.
    }

    @Override
    protected void onStop() {
        mViolationDriverList.clear();
        super.onStop();
    }

    @Override
    public void finish() {
        mViolationDriverList.clear();
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    @Override
    public void onViolationDriverclick(int position) {
        violationFormUid = mViolationDriverList.get(position).getViolation_form_uid();
        violationFormPenalty  = mViolationDriverList.get(position).getViolation_penalty();
        if (mViolationDriverList.get(position).getPayment_status().equals("unpaid")) {
            processPayment(Float.valueOf(mViolationDriverList.get(position).getViolation_penalty()));
        } else {
            Toast.makeText(violation.this, "Violation is already paid ", Toast.LENGTH_SHORT).show();
        }
    }

    private void processPayment(Float amount) {
//        Toast.makeText(violation.this, "amount " + amount, Toast.LENGTH_SHORT).show();
        totalAmount = amount + (amount * 0.01);
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(totalAmount), "₱", "Penalty and 2% for service fee", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(violation.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null) {
                    try {
                        String paymentDetails = confirmation.toJSONObject().toString(4);
                        startActivity(new Intent(violation.this, paymentDetails.class)
                                .putExtra("agencyDocUid", agencyDocUid)
                                .putExtra("violationFormUid", violationFormUid)
                                .putExtra("driver_uid", user_uid)
                                .putExtra("violation_penalty", violationFormPenalty)
                                .putExtra("paymentDetails", paymentDetails)
                                .putExtra("paymentAmount", totalAmount)
                        );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (resultCode == Activity.RESULT_CANCELED)
                    Toast.makeText(violation.this, "Cancel", Toast.LENGTH_SHORT).show();

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(violation.this, "Invalid", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
