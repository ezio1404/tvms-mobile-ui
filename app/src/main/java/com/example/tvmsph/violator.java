package com.example.tvmsph;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class violator extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference agencyRef = db.collection("agency");
    private CollectionReference formRef = db.collection("form");
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressBar mProgresbar;
    private static final String TAG = "violator";
    private Button scan_btn, addViolator;
    private EditText mQRcode;
    private TextInputEditText fname, lname, mi, plateNo, desc, location;
    private CheckBox checkbox;
    private StorageReference mStoreRef;
    private Button mClearButton;
    private SignaturePad mSignaturePad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_violator);
        getSupportActionBar().setTitle("Violator / Add Violator");
        mProgresbar = findViewById(R.id.progress_violator);
        scan_btn = (Button) findViewById(R.id.btn_scan);
        mQRcode = findViewById(R.id.mQRcode);
        addViolator = findViewById(R.id.addViolator);
        fname = findViewById(R.id.violator_firstname);
        lname = findViewById(R.id.violator_lastname);
        mi = findViewById(R.id.violator_mi);
        plateNo = findViewById(R.id.violator_plateNo);
        desc = findViewById(R.id.violator_description);
        location = findViewById(R.id.violator_locationOfIncident);
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mClearButton = findViewById(R.id.btnClearSign);
        mStoreRef = FirebaseStorage.getInstance().getReference();
//        checkbox = findViewById(R.id.checkBox);
        mAuth = FirebaseAuth.getInstance();
        final Activity activity = this;
        scan_btn.setOnClickListener(view -> {
            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            integrator.setPrompt("Scan");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();
        });
        Calendar calendar = Calendar.getInstance();
        int thisMonth = calendar.get(Calendar.MONTH);
        Log.d(TAG, "@ thisMonth : " + thisMonth);
//        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if (b) {
//                    addViolator.setEnabled(true);
//                } else {
//                    addViolator.setEnabled(false);
//                }
//            }
//        });
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {

            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
                Toast.makeText(violator.this, "OnStartSigning", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
                addViolator.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mClearButton.setEnabled(false);
                addViolator.setEnabled(false);
                //Event triggered when the pad is cleared
            }
        });
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignaturePad.clear();
            }
        });

        mAuthListener = firebaseAuth -> {
            FirebaseUser user = mAuth.getCurrentUser();
            if (user != null) {
                addViolator.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bitmap bitmap = mSignaturePad.getSignatureBitmap();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] dataPhoto = baos.toByteArray();


                        mProgresbar.setVisibility(View.VISIBLE);
                        addViolator.setVisibility(View.GONE);
                        String driver_fname = fname.getText().toString().trim();
                        String driver_lname = lname.getText().toString().trim();
                        String driver_mi = mi.getText().toString().trim();

                        String license_no = mQRcode.getText().toString().trim().replaceAll("\\s+", "").toUpperCase();

                        String locationOfIncedent = location.getText().toString().trim();
                        String description = desc.getText().toString().trim();
                        String vehicle_plateNo = plateNo.getText().toString().trim().replaceAll("\\s+", "").toUpperCase();


                        Date cdarte = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
//                        String formattedDate = df.format(cdarte);

                        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());


                        Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH) + 1;
                        int mDay = c.get(Calendar.DAY_OF_MONTH);
                        String thisMonthAndYear = mMonth + "-" + mYear;
                        // display the current date
                        String CurrentDate = mYear + "-" + mMonth + "-" + mDay;
                        String formattedDate = mYear + "-" + mMonth + "-" + mDay;
                        String expiryDate = CurrentDate; // Start date
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                        c = Calendar.getInstance();

                        try {
                            c.setTime(sdf.parse(expiryDate));
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        c.add(Calendar.DATE, 15);//insert the number of days you want to be added to the current date
                        sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date resultdate = new Date(c.getTimeInMillis());
                        expiryDate = sdf.format(resultdate);
                        String finalExpiryDate = expiryDate;

                        if (driver_fname.isEmpty()) {
                            fname.setError("Please Insert Field");
                            mProgresbar.setVisibility(View.GONE);
                            addViolator.setVisibility(View.VISIBLE);
                        }
                        if (driver_lname.isEmpty()) {
                            lname.setError("Please Insert Field");
                            mProgresbar.setVisibility(View.GONE);
                            addViolator.setVisibility(View.VISIBLE);
                        }
                        if (license_no.isEmpty()) {
                            mQRcode.setError("Please Insert Field");
                            mProgresbar.setVisibility(View.GONE);
                            addViolator.setVisibility(View.VISIBLE);
                        }
                        if (vehicle_plateNo.isEmpty()) {
                            plateNo.setError("Please Insert Field");
                            mProgresbar.setVisibility(View.GONE);
                            addViolator.setVisibility(View.VISIBLE);
                        }
                        if (vehicle_plateNo.isEmpty() && license_no.isEmpty() && driver_lname.isEmpty() && driver_fname.isEmpty()) {
                            Toast.makeText(violator.this, "PLease input fields", Toast.LENGTH_SHORT).show();
                        }
                        if (!vehicle_plateNo.isEmpty() && !license_no.isEmpty() && !driver_lname.isEmpty() && !driver_fname.isEmpty()) {
                            agencyRef.whereEqualTo("subscription_status", "active").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                    if (e != null) {
                                        System.err.println("Listen failed:" + e);
                                        return;
                                    }
                                    List<String> agency = new ArrayList<>();

                                    for (DocumentSnapshot doc : snapshots) {
                                        if (doc.get("agency_name") != null) {
                                            agency.add(doc.getString("agency_name"));

                                            agencyRef.document(doc.getId()).collection("enforcer")
                                                    .whereEqualTo("status", "enforcer")
                                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                        @SuppressLint("SetTextI18n")
                                                        @Override
                                                        public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                                                            if (e != null) {
                                                                System.err.println("Listen failed:" + e);
                                                                return;
                                                            }
                                                            List<String> enforcerList = new ArrayList<>();
                                                            for (DocumentSnapshot enfDoc : snapshots) {
                                                                Enforcer enforcer = enfDoc.toObject(Enforcer.class);
                                                                if (enfDoc.get("enforcer_lname") != null) {
                                                                    enforcerList.add(enfDoc.getId());
                                                                    if (enfDoc.getId().equals(user.getUid())) {
                                                                        ///////////////////////////////////////////////
                                                                        formRef.document(user.getUid()).collection("violation").orderBy("violation_desc")
                                                                                .get()
                                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                                        if (task.isSuccessful()) {
                                                                                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                                                                                Log.d(TAG, document.getId() + " => " + document.getData());
//

                                                                                                Map<String, Object> data = new HashMap<>();
                                                                                                data.put("license_no", license_no);
                                                                                                data.put("violation_uid", document.getId());
                                                                                                data.put("vehicle_plateNo", vehicle_plateNo);
                                                                                                data.put("enforcer_uid", user.getUid());
                                                                                                data.put("violation_dateInMillis", System.currentTimeMillis());
                                                                                                data.put("violation_date", formattedDate);
                                                                                                data.put("violation_time", currentTime);
                                                                                                data.put("violation_desc", description);
                                                                                                data.put("violation_dateCreated", String.valueOf(new Date()));
                                                                                                data.put("violation_location", locationOfIncedent);
                                                                                                data.put("payment_status", "unpaid");
                                                                                                data.put("violation_dueDate", finalExpiryDate);
                                                                                                agencyRef.document(doc.getId()).collection("violation_forms").add(data).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                                                                                    @Override
                                                                                                    public void onComplete(@NonNull Task<DocumentReference> taskViola) {
                                                                                                        Log.d(TAG, "new violation : " + taskViola.getResult().getId());
                                                                                                        StorageReference storageRef = mStoreRef.child(user.getUid() + "/form_violation/" + taskViola.getResult().getId());
                                                                                                        Uri uri = Uri.parse(user.getUid() + "/form_violation/" + taskViola.getResult().getId() + ".jpg");
                                                                                                        Log.d(TAG, "onComplete: " + uri.toString());
//                                                                                                        storageRef.putBytes(dataPhoto);
                                                                                                        StorageTask uploadTask = storageRef.putBytes(dataPhoto);
                                                                                                        Task urlTask =uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                                                                                            @Override
                                                                                                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                                                                                if (!task.isSuccessful()) {
                                                                                                                    throw task.getException();
                                                                                                                }
                                                                                                                // Continue with the task to get the download URL
                                                                                                                return storageRef.getDownloadUrl();
                                                                                                            }
                                                                                                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                                                                            @Override
                                                                                                            public void onComplete(@NonNull Task<Uri> task) {
                                                                                                                if (task.isSuccessful()) {
                                                                                                                    Uri downloadUri = task.getResult();
                                                                                                                    Log.d(TAG, "onComplete: " + downloadUri.toString());
                                                                                                                    Map<String, Object> violationData = new HashMap<>();
                                                                                                                    violationData.put("signature_photoRef", downloadUri.toString());
                                                                                                                    agencyRef.document(doc.getId()).collection("violation_forms").document(taskViola.getResult().getId()).update(violationData).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                                        @Override
                                                                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                                                                            Toast.makeText(violator.this, "Added Violator Signature", Toast.LENGTH_SHORT).show();
                                                                                                                        }
                                                                                                                    });
                                                                                                                } else {
                                                                                                                    Toast.makeText(violator.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                                                                                }
                                                                                                            }
                                                                                                        });

                                                                                                                formRef.document(user.getUid()).collection("violation").document(document.getId()).delete();
                                                                                                    }
                                                                                                });

                                                                                            }
                                                                                        } else {
                                                                                            Log.d(TAG, "Error getting documents: ", task.getException());
                                                                                        }
                                                                                    }
                                                                                });

                                                                        ////////////////////////////////////
                                                                        agencyRef.document(doc.getId()).collection("vehicle").orderBy("vehicle_plateNo")
                                                                                .get()
                                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                                        if (task.isSuccessful()) {
                                                                                            boolean isNotTheSame = false;
                                                                                            String uid = null;
                                                                                            float violationCount = 0;

                                                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                                                Log.d(TAG, document.getId() + " => " + document.get("vehicle_plateNo") + " == " + vehicle_plateNo);
                                                                                                if (!document.get("vehicle_plateNo").equals(vehicle_plateNo)) {
                                                                                                    isNotTheSame = true;
                                                                                                } else {
                                                                                                    uid = document.getId();
                                                                                                    violationCount = Float.parseFloat(String.valueOf(document.get("vehicle_violationCount")));
//                                                                                                    Log.d(TAG, "vehicle uid: " + uid);
//                                                                                                    Log.d(TAG, "violationCount bvnm: " + violationCount);
                                                                                                    Map<String, Object> vehicleData = new HashMap<>();
                                                                                                    vehicleData.put("vehicle_updatedAt", String.valueOf(new Date()));
                                                                                                    vehicleData.put("vehicle_violationCount", violationCount + 1);
                                                                                                    agencyRef.document(doc.getId())
                                                                                                            .collection("vehicle").document(uid).update(vehicleData);
                                                                                                    return;
                                                                                                }
                                                                                            }
                                                                                            Map<String, Object> vehicleData = new HashMap<>();
                                                                                            if (isNotTheSame) {
                                                                                                vehicleData.put("vehicle_brand", "");
                                                                                                vehicleData.put("vehicle_capacity", "");
                                                                                                vehicleData.put("vehicle_model", "");
                                                                                                vehicleData.put("vehicle_plateNo", vehicle_plateNo);
                                                                                                vehicleData.put("vehicle_regDate", "");
                                                                                                vehicleData.put("vehicle_regDateExpiry", "");
                                                                                                vehicleData.put("vehicle_dateCreated", String.valueOf(new Date()));
                                                                                                vehicleData.put("vehicle_status", "");
                                                                                                vehicleData.put("vehicle_type", "");
                                                                                                vehicleData.put("vehicle_violationCount", 1);
                                                                                                Log.d(TAG, "no same plateno");
                                                                                                agencyRef.document(doc.getId())
                                                                                                        .collection("vehicle").document().set(vehicleData);
                                                                                                Toast.makeText(violator.this, "Success adding new vehicle", Toast.LENGTH_SHORT).show();
                                                                                            } else {

                                                                                            }
                                                                                        } else {
                                                                                            Log.d(TAG, "Error getting documents: ", task.getException());
                                                                                        }
                                                                                    }
                                                                                });

                                                                        agencyRef.document(doc.getId()).collection("driver").orderBy("license_no")
                                                                                .get()
                                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                                        if (task.isSuccessful()) {
                                                                                            boolean isNotTheSame = false;
                                                                                            String uid = null;
                                                                                            float violationCountDriver = 0;

                                                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                                                Log.d(TAG, document.getId() + " => " + document.getData());
                                                                                                if (!document.get("license_no").equals(license_no)) {
                                                                                                    isNotTheSame = true;
                                                                                                } else {
                                                                                                    uid = document.getId();
//                                                                                                    violationCountDriver = Float.parseFloat(String.valueOf(document.get("driver_violationCount"))) + 1;
                                                                                                    Log.d(TAG, "driver uid: " + uid);
                                                                                                    Log.d(TAG, "violationCount : " + violationCountDriver);
                                                                                                    Map<String, Object> Driverdata = new HashMap<>();
                                                                                                    Driverdata.put("driver_updatedAt", String.valueOf(new Date()));
//                                                                                                    Driverdata.put("driver_violationCount", violationCountDriver);
                                                                                                    Log.d(TAG, "onComplete: " + violationCountDriver);
                                                                                                    agencyRef.document(doc.getId())
                                                                                                            .collection("vehicle").document(uid).update(Driverdata);
                                                                                                    return;
                                                                                                }
                                                                                            }

                                                                                            if (isNotTheSame) {
                                                                                                Map<String, Object> Driverdata = new HashMap<>();
                                                                                                Driverdata.put("license_no", license_no);
                                                                                                Driverdata.put("driver_fname", driver_fname);
                                                                                                Driverdata.put("driver_lname", driver_lname);
                                                                                                Driverdata.put("driver_dateCreated", String.valueOf(new Date()));
                                                                                                Driverdata.put("driver_mi", driver_mi);
//                                                                                                Driverdata.put("driver_violationCount", 1);
                                                                                                agencyRef.document(doc.getId()).collection("driver").add(Driverdata);
                                                                                            } else {

                                                                                            }
                                                                                        } else {
                                                                                            Log.d(TAG, "Error getting documents: ", task.getException());
                                                                                        }
                                                                                    }
                                                                                });

                                                                    }
                                                                }
                                                            }
                                                            System.out.println("Current enforcer active: " + enforcerList);
                                                        }
                                                    });

                                        }

                                    }
                                    System.out.println("Current Agency active: " + agency);
                                }
                            });
                        }

                        mProgresbar.setVisibility(View.GONE);
                        addViolator.setVisibility(View.VISIBLE);
                        fname.setText(null);
                        lname.setText(null);
                        mi.setText(null);
                        plateNo.setText(null);
                        desc.setText(null);
                        location.setText(null);
                        mQRcode.setText(null);
                        Toast.makeText(violator.this, "Success Adding Violator", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(violator.this, dashboard.class));
                    }

                });

            } else {

            }
        };
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_LONG).show();
            } else {
                mQRcode.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}
